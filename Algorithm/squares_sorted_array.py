def square_sorted(initial_array):
    """return an array of the squares

    Args:
        initial_array (List[int]): Given an integer array nums sorted in non-decreasing order, 

    Returns:
        int: an array of the squares of each number
    """
    squared_array = [x**2 for x in initial_array]
    return sorted(squared_array)
