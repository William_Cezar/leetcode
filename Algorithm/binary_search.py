"""
Given an array of integers nums which is sorted in ascending order, 
and an integer target, write a function to search target in nums. 
If target exists, then return its index. Otherwise, return -1.
You must write an algorithm with O(log n) runtime complexity.
"""

def search(nums, target):
    """check if an element belongs to a list and if so, return its index
    Args:
        nums (int): target number to search in the list
        target (List[int]): list with all elements
    Returns:
        int: index of the element or -1 it elements does not exist in list
    """
    if target in nums:
        return nums.index(target)
    return -1
