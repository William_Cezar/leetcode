def is_bad_version(middle_version):
    """this is already implemented, fake function
    """
    return middle_version


def first_bad_version(number_versions):
    """find out the first bad one

    Args:
        number_versions List(int): n versions

    Returns:
        int: first bad one
    """
    left = 1
    right = number_versions
    while left < right:
        mid = left + (right - left) // 2
        if is_bad_version(mid):
            right = mid
        else:
            left = mid + 1
    return left
