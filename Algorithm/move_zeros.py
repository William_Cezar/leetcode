def new_remove_zeros(nums):
    """move all 0's to the end of it while maintaining 
    the relative order of the non-zero elements.

    Args:
        nums (List[int]): integer array nums

    Returns:
        List[int]: integer array nums
    """
    amount_zeros = 0
    counter = 0
    array_size = len(nums)
    while counter < array_size:
        if nums[counter] == 0:
            amount_zeros +=1
        counter += 1
    number_to_remove = 0
    while number_to_remove in nums:
        nums.remove(number_to_remove)
    for _ in range(0, amount_zeros):
        nums.append(0)
    return nums
