def search_insert(nums, target):
    """_summary_

    Args:
        nums (List[int]): sorted array of distinct integers
        target (int): target value

    Returns:
        int: return the index
    """
    new_index = 0
    if target in nums:
        return nums.index(target)
    for index, value in enumerate(nums):
        if target > value:
            new_index = index + 1
    return new_index


list_num = [1, 3, 5, 6]
TARGET_NUM = 5

print(search_insert(list_num, TARGET_NUM))
