def reverse_words_string(word):
    """Reverse each word of a sentence

    Args:
        word string: sentence of words

    Returns:
        string: sentence reverted
    """    
    word_list = word.split()
    word_list_reverted = []
    for word_item in word_list:
        word_list_reverted.append(word_item[::-1])
    final_sentence = ' '.join(word_list_reverted)
    return final_sentence