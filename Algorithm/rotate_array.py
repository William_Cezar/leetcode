def rotate_array(nums, k):
    """rotate the array to the right by k steps
    Args:
        nums (List[int]): integer array nums
        k (int): k steps
    """
    size_array = len(nums)
    if size_array == 0 or size_array == 1:
        return

    k %= size_array
    if k == 0:
        return

    left_to_right_array = nums[:-k]
    right_to_left_array = nums[-k:]

    nums[:k] = right_to_left_array
    nums[k:] = left_to_right_array
