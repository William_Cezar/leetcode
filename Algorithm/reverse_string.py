def reverse_string(word):
    """Reverse string

    Args:
        word string: word to be reversed
    """    
    final = len(word) - 1
    for i in range(0, len(word) // 2):
        word[i], word[final] = word[final], word[i]
        final = final - 1
        