def add_two_digits(nums, target):
    """find two numbers such that they add up to a specific target number

    Args:
        nums List[int]: array of integers numbers
        target int: specific target number

    Returns:
        List[int]: indices of the two numbers
    """
    first_counter = 0
    second_counter = 1
    final_position = []
    while first_counter < len(nums):
        while second_counter < len(nums):
            if nums[first_counter] + nums[second_counter] == target:
                final_position.append(first_counter + 1)
                final_position.append(second_counter + 1)
                break
            second_counter += 1
        first_counter += 1
        second_counter = first_counter + 1
    return final_position
