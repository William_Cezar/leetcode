def contains_duplicate(numbers):
    """Check if value appears twice

    Args:
        numbers List[int]: integer array

    Returns:
        boolean: true if value appears twice, false if not
    """
    if len(numbers) == len(set(numbers)):
        return False
    return True
